FROM php:7.4-cli-alpine

RUN apk update \
    && docker-php-ext-install pdo_mysql \
    && rm -rf '/var/cache/apk/*' \
    && mkdir -p /reaper

WORKDIR /reaper
COPY . .

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN composer install
